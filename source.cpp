#include <vector>
#include <iostream>
#include <string>
using namespace std;

int main()
{

//Vectors
vector<char> v;
vector<int> v1 = {10, 14, 32, 64, 16};

//Strings
string str1 = "hello";
string str2 = str1 + " world";

//Vector Code
v.push_back('p'); // add/push a new c har at the back of v
v1.pop_back(); // remove one element from v1 (16)

v.push_back('i');
v1.pop_back();


// Looping through Vectors
for (int i = 0; i < 4; i++)// loop 4 times
{
	v1.push_back(0); // add a 0 to the end of vector each loop
}

for (int i = 0; i < v1.size(); i++) // loop through vector
{
	cout << v1[i] << " "; // outut vector
}

// String Code
// Printing strings
cout << endl << endl;
cout << "str1 = " << str1 << endl; // print out string 
cout << "the 4th character is " << str1[3] << endl; // print out 4th letter in string
cout << "strl has " << str1.size() << " characters" << endl; // print out the amount of characters in string

// Comparing Strings
cout << endl;
if (str1 == "hello") cout << "hi there " << endl; // compare if string matches
if (str2 != "world") cout << "out of this world" << endl;

// Appending Strings
str2 += "ly one"; // same as str2 = str2 + "lu one"
cout << str2 << endl << endl;

// For-Each code
// Standard loop code
for(unsigned i = 0; i < str1.size(); i++)
{
	cout << str1[i];
}

cout << endl;

// Alternative to standard for loop
for( char ch: str1) // For each character in the string str1
{
	cout << ch;
}

cout << endl;

// Code for auto

//decleration
auto i = 0; // i is deduced to be an int
auto x = 0u;// x is deduced to be unsigned int, used for warning : comparison of singed and unsigned interger expressions
auto c = str1[2]; // c is deduced to be pf type char
auto a = v1; // a is deduced to be of type vector<int>

for(auto ch: str1) // same as above, for each char in the string str1
{
	cout << ch;
}

cout << endl;

for(auto v: v1) //for each interger in the vector v1
{
	cout << v;
}

cout << endl;



}
